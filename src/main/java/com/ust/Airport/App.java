package com.ust.Airport;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.Scanner;

import javax.naming.directory.SearchControls;

import com.ust.dao.daoImpl;

import com.ust.model.ClientInfo;

public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	BufferedReader reader=new BufferedReader(new InputStreamReader(System.in));
    	daoImpl dimp = new daoImpl();
    	System.out.println("Enter the number of clients :");
    	Scanner sc=new Scanner(System.in);
    	int num=sc.nextInt();
    	int count = 1;
    	for(int i=1;i<=num;i++) {
    		System.out.println("Enter the details of the client  "+count);
    		ClientInfo client=new ClientInfo();
    		client.setName(reader.readLine());
    		client.setEmail(reader.readLine());
    		String passportNumber =reader.readLine();
    		boolean status=dimp.insertClientDetails(passportNumber, client);
    		count++;
    			
    	}
    	Map<String, ClientInfo> cmap = dimp.getAllClientDetails();
		System.out.println("Enter the passport number of the client to be searched ");
		String serachKey=reader.readLine();
		System.out.println("Client Details");
		if(cmap.containsKey(serachKey)) {
			ClientInfo cmp = cmap.get(serachKey);
			System.out.println(cmp.getName() +"--" + cmp.getEmail()+"--"+ serachKey);
		}
		else {
			System.out.println("Employee is not found");
		}

    	
    	
    }
}
